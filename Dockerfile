FROM tensorflow/tensorflow:2.7.0-gpu-jupyter

RUN apt-get update -y && apt-get upgrade -y
RUN apt-get install python3.7 -y
RUN apt-get install python3-pip -y
RUN pip install numpy~=1.21.5
RUN pip install pandas~=1.3.5
RUN pip install biopython
RUN pip install protein-bert
RUN pip install sklearn
RUN pip install nbconvert
RUN pip install seaborn
RUN pip install umap-learn

WORKDIR protein-bert/