import pandas as pd
import numpy as np
import proteinbert
from proteinbert import load_pretrained_model
from proteinbert.conv_and_global_attention_model import get_model_with_hidden_layers_as_outputs

imgt_a = pd.read_csv("./data/IMGT_Hs_Va_germline.csv")
imgt_b = pd.read_csv("./data/IMGT_Hs_Vb_germline.csv")

seq_len = 128
batch_size = 32


pretrained_model_generator, input_encoder = load_pretrained_model("./models", "epoch_92400_sample_23500000.pkl")
model = get_model_with_hidden_layers_as_outputs(pretrained_model_generator.create_model(seq_len))
X = input_encoder.encode_X(imgt_a["cdr1"].tolist(), seq_len)

cdr1a_local, cdr1a_global = model.predict(X, batch_size = batch_size)

X = input_encoder.encode_X(imgt_a["cdr2"].tolist(), seq_len)
cdr2a_local, cdr2a_global = model.predict(X, batch_size = batch_size)

### get column means of 3d numpy array 3d -> 2d and write to file
alpha_local = np.concatenate((cdr1a_local.mean(axis=1), cdr2a_local.mean(axis=1)), axis=1)
np.savetxt("./data/IMGT_Hs_Va_germline_pBERT_local.csv", alpha_local, delimiter=",")

alpha_global = np.concatenate((cdr1a_global, cdr2a_global), axis=1)
np.savetxt("./data/IMGT_Hs_Va_germline_pBERT_global.csv", alpha_global, delimiter=",")


X = input_encoder.encode_X(imgt_b["cdr1"].tolist(), seq_len)
cdr1b_local, cdr1b_global = model.predict(X, batch_size = batch_size)

X = input_encoder.encode_X(imgt_b["cdr2"].tolist(), seq_len)
cdr2b_local, cdr2b_global = model.predict(X, batch_size = batch_size)

beta_local = np.concatenate((cdr1b_local.mean(axis=1), cdr2b_local.mean(axis=1)), axis=1)
np.savetxt("./data/IMGT_Hs_Vb_germline_pBERT_local.csv", beta_local, delimiter=",")

beta_global = np.concatenate((cdr1b_global, cdr2b_global), axis=1)
np.savetxt("./data/IMGT_Hs_Vb_germline_pBERT_global.csv", beta_global, delimiter=",")

