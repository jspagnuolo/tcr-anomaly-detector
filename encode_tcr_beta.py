import pandas as pd
import numpy as np
import os
from proteinbert import load_pretrained_model
from proteinbert.conv_and_global_attention_model import get_model_with_hidden_layers_as_outputs

seq_len = 256
batch_size = 32
pretrained_model_generator, input_encoder = load_pretrained_model("./models", "epoch_92400_sample_23500000.pkl")
model = get_model_with_hidden_layers_as_outputs(pretrained_model_generator.create_model(seq_len))

for i in range(1, 95):
    seqs = pd.read_csv(os.path.join("./data/tcr_beta/tcr_beta_seq_" + str(i) + ".csv"))
    X = input_encoder.encode_X(seqs["seq"].tolist(), seq_len)
    local_embedding, global_embedding = model.predict(X, batch_size=batch_size)
    #np.save(os.path.join("./data/tcr_beta/pbert/tcr_beta_seq_" + str(i) + "_pBERT_local.npy"), local_embedding)
    np.savetxt(os.path.join("./data/tcr_beta/pbert/tcr_beta_seq_" + str(i) + "_pBERT_local.csv"), local_embedding.mean(axis=1), delimiter=",")
    #np.savetxt(os.path.join("./data/tcr_beta/pbert/tcr_beta_seq_" + str(i) + "_pBERT_global.csv"), global_embedding, delimiter=",")
