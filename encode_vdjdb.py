import pandas as pd
import numpy as np
import os
from proteinbert import load_pretrained_model
from proteinbert.conv_and_global_attention_model import get_model_with_hidden_layers_as_outputs

seq_len = 128
batch_size = 32
pretrained_model_generator, input_encoder = load_pretrained_model("./models", "epoch_92400_sample_23500000.pkl")
model = get_model_with_hidden_layers_as_outputs(pretrained_model_generator.create_model(seq_len))

for i in range(1,163):
    seqs = pd.read_csv(os.path.join("./data/MR1neg/tcr_vbeta_seq_" + str(i) + ".csv"))
    X = input_encoder.encode_X(seqs["cdr1"].tolist(), seq_len)
    cdr1b_local, cdr1b_global = model.predict(X, batch_size = batch_size)
    X = input_encoder.encode_X(seqs["cdr2"].tolist(), seq_len)
    cdr2b_local, cdr2b_global = model.predict(X, batch_size = batch_size)
    beta_local = np.concatenate((cdr1b_local.mean(axis=1), cdr2b_local.mean(axis=1)), axis=1)
    beta_global = np.concatenate((cdr1b_global, cdr2b_global), axis=1)
    X = input_encoder.encode_X(seqs["cdr3"].tolist(), seq_len)
    cdr3b_local, cdr3b_global = model.predict(X, batch_size = batch_size)
    np.savetxt(os.path.join("./data/MR1neg/pbert/tcr_vbeta_cdr3b_" + str(i) + "_pBERT_local.csv"), cdr3b_local.mean(axis=1), delimiter=",")
    np.savetxt(os.path.join("./data/MR1neg/pbert/tcr_vbeta_cdr3b_" + str(i) + "_pBERT_global.csv"), cdr3b_global, delimiter=",")
    np.savetxt(os.path.join("./data/MR1neg/pbert/tcr_vbeta_" + str(i) + "_pBERT_local.csv"), np.concatenate((beta_local, cdr3b_local.mean(axis=1)), axis=1), delimiter=",")
    np.savetxt(os.path.join("./data/MR1neg/pbert/tcr_vbeta_" + str(i) + "_pBERT_global.csv"), np.concatenate((beta_global, cdr3b_global), axis=1), delimiter=",")