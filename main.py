import numpy as np
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers



### Encode with proteinBERT, run latent variable through UMAP/tSNE check number of clusters and positions of MAIT/MR1T vs other pops


### Train VAE on MAIT/MR1T data (proteinBERT encoded)
### Find cosine similarity between input and output - set threshold very high to id MAIT *and* MR1T
### Secondary classifier trained on MR1T and MAIT to pull out the MR1T?
