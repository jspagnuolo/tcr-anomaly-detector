---
title: "2021110_TCR_Anomaly_Detector_Data_Sanitizing_JS"
author:
  name: Julian Spagnuolo
date: "2021/11/10"
output:
  html_document:
    df_print: paged
    fig_height: 10
    fig_width: 10
    number_sections: yes
    theme: spacelab
    toc: yes
    toc_depth: '6'
    toc_float: yes
description: |
  Reformatting all training and test data to have a common format for easy access.
categories:
- TCR
- Discovery
bibliography: null
---

```{r setup, echo=FALSE, cache=FALSE, results='hide', tidy=TRUE, split=FALSE, collapse=TRUE, include=FALSE}
library(knitr)

library(formatR)

# plotting and data wrangling
library(ggplot2)
library(ggalt)
library(ggsci)
library(ggthemes)
library(ggrepel)
library(gridExtra)
library(ggbeeswarm)
library(ggsignif)
library(ggpubr)
library(ggExtra)
library(ggstatsplot)
library(ggfortify)
library(viridis)

library(stringr)
library(reshape2)
library(caret)

mtb_pal <- c("#CCE7CE", "#9FD3A7", "#3DAE8A", "#01877F", "#00416B")


## Global options
set.seed(42)
options(max.print="200")
opts_chunk$set(cache=TRUE,
               prompt=FALSE,
               tidy=TRUE,
               comment=NA,
               message=FALSE,
               collapse=TRUE,
               layout="l-body-outset",
               R.options = list(width = 80),
               autodep = TRUE,
               echo=TRUE,
               split=FALSE,
               cache.extra = rand_seed,
               optipng = '-o4',
               dpi=96, figure.retina=1)
opts_knit$set(width=75)
```


# Intro

Primary project folder is located:

```{r}
getwd()
```


# McPAS-TCR Data

Entire human database retrieved on 2021/11/10

Here's the Antigen identification methods index:

1 Peptide-MHC (pMHC) tetramers
2 In vitro stimulation, sub-divided by the type of antigen used for the stimulation:
2.1 Stimulation with a peptide
2.2 Stimulation with a protein
2.3 Stimulation with a pathogen (could contain some MAIT, CD1 and MR1T)
2.4 Stimulation with tumor cells (these could include some MR1T)
2.5 Other types of in vitro stimulation
3 Isolated from a specific tissue under a specific pathology / condition (for example, tumor infiltrating T cells), sub-divided by method of TCR sequencing:

Import dataset, remove any useless columns. Get rid of anything without CDR3-beta sequences, set min CDR3-beta length at 5 because any smaller is likely bs anyway.

```{r}
mcpas <- read.csv(file="../data/20211110_McPAS-TCR.csv", header = T)
mcpas <- mcpas[,c("CDR3.alpha.aa","CDR3.beta.aa","Category","Pathology","Antigen.identification.method","Antigen.protein","Epitope.peptide", "MHC","Tissue","T.cell.characteristics","T.Cell.Type","TRAV","TRAJ","TRBV","TRBJ","Single.cell","NGS")]
mcpas <- mcpas[which(nchar(mcpas$CDR3.beta.aa) >= 5),]
mcpas[grep("\xa0", mcpas$CDR3.alpha.aa),"CDR3.alpha.aa"] <- str_split(mcpas[10744,"CDR3.alpha.aa"], pattern = "\xa0", simplify = T)[,1]
mcpas <- mcpas[!is.na(mcpas$TRBV),]
mcpas <- mcpas[!is.na(mcpas$TRBJ),]
mcpas <- mcpas[!is.na(mcpas$CDR3.beta.aa),]
```

```{r}
table(mcpas$Category,mcpas$Antigen.identification.method)
```

Removed all entries without defined TRBV and TRBJ genes since this information could be important in training the models.

```{r}
x <- table(mcpas[which(mcpas$TRAV == "TRAV1-2" & mcpas$TRAJ %in% c("TRAJ33","TRAJ12","TRAJ20")),c("TRBV","TRBJ")])
x
```

Looks like there could be MAIT cells here.

```{r}
mcpas[which(mcpas$TRAV == "TRAV1-2" & mcpas$TRAJ %in% c("TRAJ33","TRAJ12","TRAJ20") & mcpas$TRBV %in% c("TRBV6-1","TRBV6-4","TRBV20-1")),c("CDR3.alpha.aa","Category","Antigen.identification.method")]
```

Behold, MAIT cells! One from a Cancer pMHC selection study?!?

```{r}
mcpas[which(mcpas$TRAV == "TRAV1-2" & mcpas$TRAJ %in% c("TRAJ33","TRAJ12","TRAJ20")),c("CDR3.alpha.aa","TRBV","TRBJ","Category","Antigen.identification.method")]
mcpas_mait <- rownames(mcpas[which(mcpas$TRAV == "TRAV1-2" & mcpas$TRAJ %in% c("TRAJ33","TRAJ12","TRAJ20")),c("CDR3.alpha.aa","TRBV","TRBJ","Category","Antigen.identification.method")])
```

likely a few more MAIT in the database, but not so many as to present a major problem.... But perhaps if there is not a lot of diversity in the CDR3-betas then it could be problematic.

Remove confirmed MAIT, ie those with TRAV1-2, and TRBV6/TRBV20, identified in chunks above.

```{r}
mcpas <- mcpas[-which(rownames(mcpas) %in% mcpas_mait),]
```

Remove anything without CDR3-beta sequences, and TRBV information

```{r}
nrow(mcpas)
mcpas <- mcpas[which(nchar(mcpas$CDR3.beta.aa) != 0),]
mcpas <- mcpas[!is.na(mcpas$TRBV),]
nrow(mcpas)
```

Clean the TRBV gene names, replace colons with * to match IMGT nomenclature.


Remove leading zeros from gene family id, remove any TCRs with ambiguous TRBV usage (ie. multiple calls)

```{r}
mcpas$TRBV <- str_replace(mcpas$TRBV, pattern = ":", replacement = "\\*")
mcpas <- mcpas[grep("/TRBV", mcpas$TRBV, invert = T),]
mcpas$TRBV <- str_replace(mcpas$TRBV, "TRBV0", "TRBV")
mcpas <- mcpas[grep(pattern = "(\\/\\d+-\\d+)", mcpas$TRBV, invert = T),]
mcpas <- mcpas[grep(pattern = ",", mcpas$TRBV, invert = T),]
mcpas <- mcpas[grep(pattern = "(\\/\\d+)", mcpas$TRBV, invert = T),]

mcpas$TRBJ <- str_replace(mcpas$TRBJ, pattern = ":", replacement = "\\*")
mcpas$TRBJ <- str_replace(mcpas$TRBJ, "-0", "-")
mcpas$TRBJ <- str_replace(mcpas$TRBJ, "TRBJ0", "TRBJ")
```

```{r}
write.csv(mcpas, file="../data/mcpas_TCR_clean.csv", quote = F, row.names = F)
```

Get rid of duplicate TRBV/CDR3b combos

```{r}
mcpas_beta <- unique(mcpas[,c("TRBV","TRBJ","CDR3.beta.aa")])
write.csv(mcpas_beta, file="../data/mcpas_TCR_beta_clean.csv", quote = F, row.names = F)
```






# VDJdb

```{r}
vdjdb <- data.table::fread(file="../data/VDJdb_hsap_CDR3ab-2021-11-12 09 42 47.706.tsv", header=T, data.table= F, check.names = T, strip.white = T)
```

Split data into solo and paired alpha and beta, recombine into dataframe similar to McPAS.

```{r}
vdjdb.solo.a <- vdjdb[which(vdjdb$complex.id == 0 & vdjdb$Gene == "TRA"),c("complex.id","CDR3","V","J","MHC.A", "MHC.B", "MHC.class", "Epitope", "Epitope.gene", "Epitope.species", "Reference")]
vdjdb.solo.b <- vdjdb[which(vdjdb$complex.id == 0 & vdjdb$Gene == "TRB"),c("complex.id","CDR3","V","J","MHC.A", "MHC.B", "MHC.class", "Epitope", "Epitope.gene", "Epitope.species", "Reference")]

vdjdb.a <- vdjdb[which(vdjdb$complex.id != 0 & vdjdb$Gene == "TRA"),c("complex.id","CDR3","V","J","MHC.A", "MHC.B", "MHC.class", "Epitope", "Epitope.gene", "Epitope.species", "Reference")]
vdjdb.b <- vdjdb[which(vdjdb$complex.id != 0 & vdjdb$Gene == "TRB"),c("complex.id","CDR3","V","J","MHC.A", "MHC.B", "MHC.class", "Epitope", "Epitope.gene", "Epitope.species", "Reference")]

head(vdjdb.solo.a)
## Fix colnames and add in missing ones to the solos, reset solo complex ID to NA, - we only need to see that the CDR3 for either alpha or beta is NA to know they were single chain sequences (or from bulk).
colnames(vdjdb.solo.a)[2:4] <- c("CDR3.alpha", "TRAV", "TRAJ")
vdjdb.solo.a$CDR3.beta <- NA
vdjdb.solo.a$TRBV <- NA
vdjdb.solo.a$TRBJ <- NA
colnames(vdjdb.solo.b)[2:4] <- c("CDR3.beta", "TRBV", "TRBJ")
vdjdb.solo.b$CDR3.alpha <- NA
vdjdb.solo.b$TRAV <- NA
vdjdb.solo.b$TRAJ <- NA

colnames(vdjdb.a)[2:4] <- c("CDR3.alpha", "TRAV", "TRAJ")
colnames(vdjdb.b)[2:4] <- c("CDR3.beta", "TRBV", "TRBJ")
```

```{r}
vdjdb.pairs <- merge(vdjdb.a, vdjdb.b[,c("complex.id","CDR3.beta","TRBV","TRBJ")], by.x="complex.id", by.y="complex.id")[,c("complex.id","CDR3.alpha","TRAV","TRAJ","CDR3.beta","TRBV","TRBJ","MHC.A", "MHC.B","MHC.class","Epitope","Epitope.gene","Epitope.species","Reference")]
head(vdjdb.pairs)
```


```{r}
vdjdb.solo <- rbind(vdjdb.solo.a[,c("complex.id","CDR3.alpha","TRAV","TRAJ","CDR3.beta","TRBV","TRBJ","MHC.A", "MHC.B","MHC.class","Epitope","Epitope.gene","Epitope.species","Reference")],
                    vdjdb.solo.b[,c("complex.id","CDR3.alpha","TRAV","TRAJ","CDR3.beta","TRBV","TRBJ","MHC.A", "MHC.B","MHC.class","Epitope","Epitope.gene","Epitope.species","Reference")])

vdjdb <- rbind(vdjdb.solo, vdjdb.pairs)
```

Check for MAIT in paired data.

```{r}
 ##& vdjdb.pairs$TRBV %in% c("TRBV6-1*01","TRBV6-4*01","TRBV20-1*01")
pheatmap::pheatmap(table(vdjdb.pairs[which(vdjdb.pairs$TRAV == "TRAV1-2*01" & vdjdb.pairs$TRAJ %in% c("TRAJ33*01","TRAJ12*01","TRAJ20*01")),c("TRBJ","TRBV")]), cellwidth = 9, cellheight = 9, border_color = NA, color=viridis(100))

```

MAIT are relatively common in this dataset.

```{r}
vdjdb.pairs[which(vdjdb.pairs$TRAV == "TRAV1-2*01" & vdjdb.pairs$TRAJ %in% c("TRAJ33*01","TRAJ12*01","TRAJ20*01") & vdjdb.pairs$TRBV %in% c("TRBV6-1*01","TRBV6-4*01","TRBV20-1*01")),]

vdjdb.pairs <- vdjdb.pairs[-which(vdjdb.pairs$TRAV == "TRAV1-2*01" & vdjdb.pairs$TRAJ %in% c("TRAJ33*01","TRAJ12*01","TRAJ20*01") & vdjdb.pairs$TRBV %in% c("TRBV6-1*01","TRBV6-4*01","TRBV20-1*01")),]
```

Some of these look like canonical MAIT, others not so much..... can't be super sure outside of the alpha chain.

```{r}
vdjdb <- rbind(vdjdb.solo, vdjdb.pairs)
```



```{r}
rm(vdjdb.a); rm(vdjdb.b); rm(vdjdb.pairs); rm(vdjdb.solo.a); rm(vdjdb.solo.b)
```

# IEDB


```{r}
iedb <- data.table::fread(file="../data/IEDB_20211112_tcell_receptor_table_export.csv", header=T, data.table= F, check.names = T, strip.white = T)
iedb <- iedb[which(iedb$Receptor.Type == "alphabeta"),]
nrow(iedb)

### Make sure we only get human sequences
iedb$Species <- paste(iedb$Chain.1.Species, iedb$Chain.2.Species, sep="_")

### Don't need antigen/epitope info for projects.
iedb <- iedb[which(iedb$Species %in% c("9606_9606","NA_9606","9606_NA")), c( "Chain.1.CDR3.Calculated", "Calculated.Chain.1.V.Gene", "Calculated.Chain.1.J.Gene", "Chain.2.CDR3.Calculated", "Calculated.Chain.2.V.Gene", "Calculated.Chain.2.J.Gene", "MHC.Allele.Names","Species")]

iedb <- iedb[which(nchar(iedb$Chain.2.CDR3.Calculated) >= 5),]
iedb[grep("MR1", iedb$MHC.Allele.Names),"MHC.Allele.Names"] <- "MR1"
iedb <- unique(iedb)
nrow(iedb)
colnames(iedb) <- c("CDR3.alpha","TRAV","TRAJ","CDR3.beta","TRBV","TRBJ","MHC.allele","Species")
```

~120,000 usable sequences.

Remove MAIT or TCRs with MHC.allele == "MR1"

```{r}
iedb <- iedb[-which(iedb$MHC.allele == "MR1"),]
```

Look for obvious maits and remove

```{r}
iedb[grep("TRAV1-2", iedb$TRAV),]
```

only two

```{r}
iedb <- iedb[-which(rownames(iedb) %in% c("387","447")),]
```


```{r}
table(iedb$TRBV)
```

Most of the iedb data do not have annotated TRBV genes.

Subset those that do for full length training (& de-dupe). De-duplicate all CDR3-betas for CDR3-beta only training.

```{r}
table(iedb$TRBV, useNA = "ifany")
table(nchar(iedb$CDR3.beta) == 0)

iedb$CDR3.beta <- paste("C",iedb$CDR3.beta,"F", sep = "")

iedb.cdr3 <- data.frame(cdr3b = unique(iedb$CDR3.beta))
iedb.full <- unique(iedb[which(nchar(iedb$TRBV) != 0), c("TRBV","TRBJ","CDR3.beta")])
```





```{r}
nrow(unique(iedb[,c("CDR3.alpha","CDR3.beta")]))
```

Some duplication caused by cross-reactivity. These can be cleaned later since the antigen information is not super important. More important to have as large and diverse a training set as possible.


Most of the MAIT cells are duplicates... maybe just remove the epitope info and keep the MHC info.... knowing the exact antigen is irrelevant to the project motives anyway.


```{r}
cdr3_beta <- data.frame(unique(c(mcpas$CDR3.beta.aa, vdjdb$CDR3.beta, iedb.full$CDR3.beta)))
nrow(cdr3_beta)
tcr_beta <- data.frame(TRBV=c(mcpas$TRBV, vdjdb$TRBV, iedb.full$TRBV),
                       TRBJ=c(mcpas$TRBJ, vdjdb$TRBJ, iedb.full$TRBJ),
                       cdr3b=c(mcpas$CDR3.beta.aa, vdjdb$CDR3.beta, iedb.full$CDR3.beta))
tcr_beta <- unique(tcr_beta)
nrow(tcr_beta)
write.csv(cdr3_beta, "../data/MR1neg_clean_unique_cdr3_beta.csv", quote = F, row.names = F)
write.csv(tcr_beta, "../data/MR1neg_clean_unique_tcr_beta.csv", quote = F, row.names = F)
```



# Clean and Format IMGT Germline for Python

```{r}
x <- read.csv(file="../data/IMGT_Hs_ab_germline.csv")

x <- x[,-1]

x.alpha <- x[which(x$chain == "A" & x$region == "V"),]
x.alpha <- x.alpha[,c("id","cdr1","cdr2")]
x.alpha$cdr1 <- str_remove_all(x.alpha$cdr1, pattern = "\\.")
x.alpha$cdr2 <- str_remove_all(x.alpha$cdr2, pattern = "\\.")

x.beta <- x[which(x$chain == "B" & x$region == "V"),]
x.beta <- x.beta[,c("id","cdr1","cdr2")]
x.beta$cdr1 <- str_remove_all(x.beta$cdr1, pattern = "\\.")
x.beta$cdr2 <- str_remove_all(x.beta$cdr2, pattern = "\\.")

write.csv(x.alpha, file = "../data/IMGT_Hs_Va_germline.csv", row.names = F, quote=F, col.names = T)
write.csv(x.beta, file = "../data/IMGT_Hs_Vb_germline.csv", row.names = F, quote=F, col.names = T)
```


# ImmuneSeq Tet Sorted Data

Several pMHC and pCD1-lipid tetramer sorted datasets were acquired from the ImmunoSeq database. Here they will be cleaned and CDR1/CDR2 sequences attached.

```{r}
files <- list.files(path = "../data/ImmuneSeq/HepB_pMHC/", pattern = "*.tsv", full.names = T)
x <- list()
for(i in 1:length(files))
{
  x[[i]] <- read.table(file=files[i], sep="\t", header=T)
}
x <- do.call(rbind, x)
x <- x[which(nchar(x$aminoAcid) > 2 & x$sequenceStatus == "In"),]
x$vMaxResolved <- str_replace(x$vMaxResolved, "TCRBV0", "TRBV")
x$vMaxResolved <- str_replace(x$vMaxResolved, "-0", "-")
x$jMaxResolved <- str_replace(x$jMaxResolved, "TCRBJ0", "TRBJ")
x$jMaxResolved <- str_replace(x$jMaxResolved, "-0", "-")

files <- list.files("../data/ImmuneSeq/HLA-A2_Tet/MazouzS_2021/", pattern="*.tsv", full.names = T)
y <- list()
for(i in 1:length(files))
{
  y[[i]] <- read.table(file=files[i], sep="\t", header=T)
}
y <- do.call(rbind, y)
y <- y[which(nchar(y$cdr3_amino_acid) > 2 & y$frame_type == "In" & nchar(y$cdr1_amino_acid) > 1 & nchar(y$cdr2_amino_acid) > 1),]
y$v_resolved <- str_replace(y$v_resolved, "TCRBV0", "TRBV")
y$v_resolved <- str_replace(y$v_resolved, "-0", "-")
y$j_resolved <- str_replace(y$j_resolved, "TCRBJ0", "TRBJ")
y$j_resolved <- str_replace(y$j_resolved, "-0", "-")
nrow(y)
y_cdr <- unique(y[,c("cdr1_amino_acid","cdr2_amino_acid","cdr3_amino_acid")])
colnames(y_cdr) <- c("cdr1","cdr2","cdr3")
nrow(y_cdr)
write.csv(y_cdr, "../data/ImmuneSeq/HLA-A2_Tet/MazouzS_2021/TCR_seqs.csv", row.names = F)
y_tcr <- unique(y[,c("v_resolved","j_resolved","cdr3_amino_acid")])
colnames(y_tcr) <- c("TRBV","TRBJ","cdr3b")

files <- list.files("../data/ImmuneSeq/HLA-A2_Tet/MillerNJ_2017/", pattern="*.tsv", full.names = T)
z <- list()
for(i in 1:length(files))
{
  z[[i]] <- read.table(file=files[i], sep="\t", header=T)
}
z <- do.call(rbind, z)
z <- z[which(nchar(z$aminoAcid) > 2 & z$sequenceStatus == "In"),]
z$vMaxResolved <- str_replace(z$vMaxResolved, "TCRBV0", "TRBV")
z$vMaxResolved <- str_replace(z$vMaxResolved, "-0", "-")
z$jMaxResolved <- str_replace(z$jMaxResolved, "TCRBJ0", "TRBJ")
z$jMaxResolved <- str_replace(z$jMaxResolved, "-0", "-")


files <- list.files("../data/ImmuneSeq/LipidTet_Tuberculosis/", pattern="*Tetramer*", full.names = T)
lipid <- list()
for(i in 1:length(files))
{
  lipid[[i]] <- read.table(file=files[i], sep="\t", header=T)
}
lipid <- do.call(rbind, lipid)
lipid <- lipid[which(nchar(lipid$aminoAcid) > 2 & lipid$sequenceStatus == "In"),]
lipid$vMaxResolved <- str_replace(lipid$vMaxResolved, "TCRBV0", "TRBV")
lipid$vMaxResolved <- str_replace(lipid$vMaxResolved, "-0", "-")
lipid$jMaxResolved <- str_replace(lipid$jMaxResolved, "TCRBJ0", "TRBJ")
lipid$jMaxResolved <- str_replace(lipid$jMaxResolved, "-0", "-")

```

dataframe y already has cdr1 and cdr2 sequences assigned. rbind the others and assign the cdr1/cdr2 sequences.

```{r}
tcr_vb <- rbind(x, z, lipid)
nrow(tcr_vb)
tcr_vb <- unique(tcr_vb[,c("vMaxResolved","aminoAcid")])
colnames(tcr_vb) <- c("TRBV","cdr3b")
nrow(tcr_vb)
```

Out of 70k TCR-betas, we get 60k unique sequences, thus should be able to double the number of non MAIT TCR-beta in the training datasets.

```{r}
tcr_vb$TRBV <- str_replace(tcr_vb$TRBV, "TRBV0", "TRBV")
tcr_vb$TRBV <- str_replace(tcr_vb$TRBV, "-0", "-")
```


```{r}
tcr_vb <- rbind(tcr_beta[,c("TRBV","cdr3b")], tcr_vb)
nrow(tcr_vb)
tcr_vb <- unique(tcr_vb)
nrow(tcr_vb)

cdr3_beta <- data.frame(cdr3b=unique(tcr_vb$cdr3b))
nrow(cdr3_beta)
```

```{r}
write.csv(cdr3_beta, "../data/MR1neg_clean_unique_cdr3_beta.csv", quote = F, row.names = F)
write.csv(tcr_vb, "../data/MR1neg_clean_unique_tcr_vbeta.csv", quote = F, row.names = F)
```

```{r}
x <- x[,c("vMaxResolved","jMaxResolved","aminoAcid")]
colnames(x) <- c("TRBV","TRBJ","cdr3b")
y <- y[,c("v_resolved","j_resolved","cdr3_amino_acid")]
colnames(y) <- c("TRBV","TRBJ","cdr3b")
z <- z[,c("vMaxResolved","jMaxResolved","aminoAcid")]
colnames(z) <- c("TRBV","TRBJ","cdr3b")
lipid <- lipid[,c("vMaxResolved","jMaxResolved","aminoAcid")]
colnames(lipid) <- c("TRBV","TRBJ","cdr3b")

tcr_beta <- rbind(tcr_beta, x, y, z, lipid)
tcr_beta <- unique(tcr_beta)
write.csv(tcr_beta, "../data/MR1neg_clean_unique_tcr_beta.csv", quote = F, row.names = F)
```







# Session Info

```{r, cache=FALSE, layout="l-body-outset"}
session.info <- sessioninfo::session_info()
session.info[[1]]
```

```{r, cache=FALSE, layout="l-body-outset"}
knitr::kable(as.data.frame(session.info[[2]])[which(session.info[[2]]$attached == T),c("loadedversion","attached","date","source")])
```

# Reuse {.appendix}

Reuse or distribution of the analysis, figures and data contained herein is strictly forbidden without direct authorization by Matterhorn Biosciences AG.

