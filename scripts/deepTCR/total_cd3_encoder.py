from DeepTCR.DeepTCR import DeepTCR_U

# Instantiate training object
DTCRU = DeepTCR_U('CD3_Model')

#Load Data from directories
DTCRU.Get_Data(directory='./TAD/somedata.csv',Load_Prev_Data=False,aggregate_by_aa=True,
               aa_column_beta=0,v_beta_column=1,j_beta_column=2)



#Train VAE
DTCRU.Train_VAE(Load_Prev_Data=False, suppress_output=True)

features = DTCRU.features
print(features.shape)


mr1t = DeepTCR_U("MR1T")

mr1t.Get_Data(directory='./TAD/MR1T_beta.csv', Load_Prev_Data=False, aggregate_by_aa=True,
              aa_column_beta=0, v_beta_column=1, j_beta_column=2)


mr1t_features = DTCRU.Sequence_Inference(beta_sequences=mr1t.beta_sequences, v_beta=mr1t.v_beta, j_beta=mr1t.j_beta)

### Need to get MR1T encoding accuracy as well....
